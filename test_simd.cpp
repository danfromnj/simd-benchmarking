#include <iostream>
#include <time.h>
#include <sys/resource.h>
#include <sys/times.h>
#include <stdlib.h>
#include <algorithm>
#include <functional>

// using -D V_SIZE=8192 instead
// #define V_SIZE 8192

typedef int v4si __attribute__ ((vector_size (V_SIZE)));
const static int RANGE = 1024;

// have to use function object for emscripten wasm backend, due to emcc bad support of C++ 11....
#ifdef __EMSCRIPTEN__
class F
{
public:
    void operator()(float sys_time, float user_time, float real_time)
    {
        const char seperator = ',';
        std::cout << sys_time << seperator 
        << user_time << seperator 
        << real_time << std::endl;
    }
};
#endif

template<typename F>
class CBenchMark
{
public:
    CBenchMark(F _f):f(_f)
    {
        this->GetBenchmarkTime(stime, utime, rtime);
    }

    ~CBenchMark()
    {
        long long _stime;
        long long _utime;
        long long _rtime;
        this->GetBenchmarkTime(_stime, _utime, _rtime);
        f((_stime - stime) / 1000.0, (_utime - utime) / 1000.0, (_rtime - rtime) / 1000.0);
    }

private:
    void GetBenchmarkTime(long long& stime, long long & utime, long long & rtime)
    {
        struct rusage r;
        getrusage(RUSAGE_SELF, &r);
        struct timespec ts;
        clock_gettime(CLOCK_MONOTONIC, &ts);
        stime = r.ru_stime.tv_sec * 1000000LL + r.ru_stime.tv_usec;
        utime = r.ru_utime.tv_sec * 1000000LL + r.ru_utime.tv_usec;
        rtime = (int64_t)ts.tv_sec * 1000000 + ts.tv_nsec / 1000;
    }

private:
    long long stime;
    long long utime;
    long long rtime;
    F f;
};

void benchmarking(int iterations, int multiple)
{
    const int count = V_SIZE/sizeof(int);

    v4si vec_a;
    v4si vec_b;

    int arr_a[count];
    int arr_b[count];

    srand(time(0));

    std::cout << "preparing data:" << std::endl;
    for(int i = 0;i < count; ++ i)
    {
        arr_a[i] = ((int*)&vec_a)[i] = (int)((rand() / (RAND_MAX * 1.0)) * RANGE);
        arr_b[i] = ((int*)&vec_b)[i] = (int)((rand() / (RAND_MAX * 1.0)) * RANGE);
    }

#ifdef __EMSCRIPTEN__
    F f;
#else
    auto f = [](float sys_time, float user_time, float real_time)->void{
        const char seperator = ',';
        std::cout << sys_time << seperator 
        << user_time << seperator 
        << real_time << std::endl;
    };
#endif

    std::cout << "benchmark with normal codes (sys_time, user_time, real_time):" << std::endl;
    int arr_c[count];
    {
    #ifdef __EMSCRIPTEN__
        CBenchMark<F> b(f);
    #else
        CBenchMark<std::function<void(float,float,float)>> b(f);
    #endif
        
        for(int m = 0; m < multiple; ++m)
        {
            for(int iter = 0;iter < iterations;  ++iter)
            {
                for(int i = 0; i < count; ++i)
                {
                    arr_c[i] = arr_a[i] * arr_b[i];
                }
            }
        }
    }

    std::cout << "benchmark with SIMD instructions (sys_time, user_time, real_time): " << std::endl;
    v4si vec_c;
    {
    #ifdef __EMSCRIPTEN__
        CBenchMark<F> b(f);
    #else
        CBenchMark<std::function<void(float,float,float)>> b(f);
    #endif
        for(int m = 0; m < multiple; ++m)
        {
            for(int iter = 0;iter < iterations;  ++iter)
            {
                vec_c = vec_a * vec_b;
            }
        }
    }
}

// compile:
// g++ test_simd.cpp -D V_SIZE=32 -o test_simd
// emcc test_simd.cpp -msimd128 -s WASM=1 -s SIMD=1 -D V_SIZE=32 -o test_simd.js
// run:
// ./test_simd 10000000
// node -experimental-wasm-simd ./test_simd.js 10000000

int main(int argc, char** argv)
{
    int iterations = argc > 1 ? atoi(argv[1]) : 1;
    int multiple = argc > 2 ? atoi(argv[2]) : 1;
    std::cout << "benchmark for vector size = " << V_SIZE/sizeof(int) << " with " << iterations << " iterations..." << std::endl;
    benchmarking(iterations,multiple);
}