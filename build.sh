#!/bin/sh
if [ "$#" -eq 1 ] && [ $1 = "wasm" ]
then
CC=emcc
OPTIONS="-msimd128 -O3 -s WASM=1 -s SIMD=1"
JS_EXT=".js"
else
CC=g++
OPTIONS="-O3 -march=corei7"
JS_EXT=""
fi

for size in 32 64 128 256 512 1024 2048 4096
do
cmd="$CC test_simd.cpp $OPTIONS -D V_SIZE=$size -o test_simd_$size$JS_EXT"
echo $cmd
$cmd
done
