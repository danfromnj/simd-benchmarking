## Build and Run
refer to the comments in front of main() function in test_simd.cpp

## Batch build
./build.sh wasm build for WebAssembly otherwise build for native codes

## Batch run
./run.sh wasm will run in node.js otherwise run as native codes
Running wasm requires node.js env

If other vector size is desired, just change the values in __for size in 32 64 128 256 512 1024 2048 4096__ for both batch.sh and run.sh