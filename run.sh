#!/bin/sh
if [ "$#" -eq 1 ] && [ $1 = "wasm" ]
then
ENGINE="node -experimental-wasm-simd "
JS_EXT=".js"
else
ENGINE=""
JS_EXT=""
fi

ITERATIONS=100000000
MULTIPLE=10000000
for size in 32 64 128 256 512 1024 2048 4096
do
cmd="$ENGINE""./test_simd_$size$JS_EXT $ITERATIONS $MULTIPLE"
echo $cmd
$cmd
done